/*//No. 1 Looping While 

var angkamulai = 0;
    console.log ('LOOPING PERTAMA');
while (angkamulai < 22) {
    angkamulai +=2;
    console.log (angkamulai + ' - I Love Coding');
}
    console.log ('LOOPPING KEDUA');
while (angkamulai > 2){
    angkamulai -=2;
    console.log (angkamulai + ' - I will become a mobile developer');
}
*/
//No. 2 Looping menggunakan for
//      SYARAT:
//      A. Jika angka ganjil maka tampilkan Santai
//      B. Jika angka genap maka tampilkan Berkualitas
//      C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka
//      ganjil maka tampilkan I Love Coding.


for (var angka=1; angka <20; angka ++) {
    if (angka ++) 
    console.log (angka + ' - santai');
    else
    console.log (angka + ' - Berkualitas')

} 

var personArr = ["John", "Doe", "male", 27]
var personObj = {
    firstName : "John",
    lastName: "Doe",
    gender: "male",
    age: 27
}
 
console.log(personArr[1]) // John
console.log(personObj.firstName) // John