//IF-ELSE

var nama1 = "John"
var nama2 = "Jane"
var nama3 = "Jenita"
var nama4 = "Junaedi"
var peran1 = " "
var peran2 = "Penyihir"
var peran3 = "Guard"
var peran4 = "Werewolf"

if (nama1 == "John" && peran1 == " ") {
    console.log ("Halo John, Pilih peranmu untuk memulai game!")
    if (nama2 == "Jane" && peran2 == "Penyihir"){
        console.log ("Selamat datang di Dunia Werewolf, Jane. Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
    if (nama3 == "Jenita" && peran3 == "Guard"){
        console.log ("Selamat datang di Dunia Werewolf, Jenita. Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
    if (nama4 == "Junaedi" && peran4 == "Werewolf"){
        console.log ("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!.")
    }}}
} else {console.log  ('Nama Harus Diisi')
}


/*
//SWITCH CASE
var hari = 21;
var bulan = 1; 
var tahun = 1945;
//  Maka hasil yang akan tampil di console adalah: '21 Januari 1945'; 
//Skeleton Code / Code yang dicontohkan yang perlu diikuti dan dimodifikasi

var tanggal; // assign nilai variabel tanggal disini! (dengan angka
antara 1 - 31)
var bulan; // assign nilai variabel bulan disini! (dengan angka antara
1 - 12)
var tahun; // assign nilai variabel tahun disini! (dengan angka antara
1900 - 2200)

*/

